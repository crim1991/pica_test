<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');
Route::get('/get-user-id', 'FrontController@getUserId');
Route::get('/get-user-logs', 'FrontController@getUserLogs');
Route::post('/download-user-log', 'FrontController@downloadUserLog');

Route::post('/save-user-log', 'FrontController@saveUserLog');

Auth::routes();

