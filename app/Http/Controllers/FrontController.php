<?php

namespace App\Http\Controllers;

use App\{Http\Classes\LogManager, UserLog};
use Illuminate\{Http\Request, Support\Facades\Auth};

class FrontController extends Controller
{
    public function __construct()
    {
        $this->middleware('check_user', ['except' => 'index']);
    }

    public function index()
    {
        return view('home');
    }

    public function getUserId()
    {
        return Auth::user()->id;
    }

    public function saveUserLog(Request $request)
    {
        $char_to_replace = ['[', ']', '"',];
        $log_id = UserLog::saveLog($char_to_replace, $request);
        return UserLog::findOrFail($log_id);
    }

    public function getUserLogs()
    {
        $user_id = Auth::user()->id;
        $logs = UserLog::where('user_id', $user_id)->get()->toArray();
        return json_encode($logs);
    }

    public function downloadUserLog(Request $request)
    {
        $user_id = $request->get('user_id');
        $log_id = $request->get('log_id');

        $log_manager = new LogManager($log_id, $user_id);

        return $log_manager->createLog() ?? 'false';
    }
}
