<?php
/**
 * Created by PhpStorm.
 * User: crist
 * Date: 30-Oct-19
 * Time: 12:47 AM
 */

namespace App\Http\Classes;

use App\UserLog;
use Illuminate\Support\Facades\Storage;

class LogManager
{
    private $user_id;
    private $log_id;
    public function __construct($log_id, $user_id)
    {
        $this->log_id = $log_id;
        $this->user_id = $user_id;
    }

    public function createLog()
    {
        $log = UserLog::getLogById($this->log_id);
        $path = "public/uploads/$this->user_id";
        $file_name = "$path/$this->log_id.txt";

        if (!file_exists($file_name)) {
            Storage::put($file_name, $log['log']);
        }

        return Storage::url($file_name);
    }
}