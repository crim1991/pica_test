<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $table = 'user_logs';
    protected $fillable = ['log', 'user_id'];

    public static function saveLog($char_to_replace, $request)
    {
        $data['log'] = str_replace($char_to_replace, '', $request->get('log'));
        $data['user_id'] = $request->get('user_id');

        return UserLog::create($data)->id;
    }

    public static function getLogById($log_id)
    {
        return UserLog::select('log')->where('id', $log_id)
            ->first()->toArray();
    }
}
