@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 offset-3">
            <div class="card">
                <div class="card-header">Pocket Calculator</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div id="calculator">
                        <app-calc></app-calc>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
